﻿using System;
using System.Collections.Generic;
using HuffmanArithmeticCoding;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class UnitTest1
    {
        InputData data = new InputData(@"..\..\..\UserData\Text.txt", @"..\..\..\UserData\Alphabet.txt");
        [TestMethod]
        public void TestHuffmanGroupBy1()
        {
            var countProb = new CountProbabilities();
            countProb.Count(data.Text, data.Alphabet);
            var probabilities = countProb.Result;

            Dictionary<string, string> codes = new Huffman(1).Code(probabilities);

            var analysis = new Analysis();
            analysis.Count(codes, probabilities, data.Alphabet.Length, data.Text.Length);
            var res = analysis.GetResult();
        }

        [TestMethod]
        public void TestHuffmanGroupBy2()
        {
            var grouping = new Grouping();
            grouping.GroupBy(data.Alphabet, 2);
            var alphabet = grouping.Result;

            var countProb = new CountProbabilities();
            countProb.Count(data.Text, alphabet);
            var probabilities = countProb.Result;

            Dictionary<string, string> codes = new Huffman(2).Code(probabilities);

            var analysis = new Analysis();
            analysis.Count(codes, probabilities, data.Alphabet.Length, data.Text.Length);
            var res = analysis.GetResult();
        }

        [TestMethod]
        public void TestHuffmanGroupBy3()
        {
            var grouping = new Grouping();
            grouping.GroupBy(data.Alphabet, 3);
            var alphabet = grouping.Result;

            var countProb = new CountProbabilities();
            countProb.Count(data.Text, alphabet);
            var probabilities = countProb.Result;

            Dictionary<string, string> codes = new Huffman(3).Code(probabilities);

            var analysis = new Analysis();
            analysis.Count(codes, probabilities, data.Alphabet.Length, data.Text.Length);
            var res = analysis.GetResult();
        }

        [TestMethod]
        public void TestArithmetic()
        {
            var probArith = new CountProbabilities();
            probArith.Count("1230", new[] { "0", "1", "2", "3" });
            var code = new Arithmetic("1230").Code(probArith.Result);
        }
    }
}

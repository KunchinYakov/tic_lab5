﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HuffmanArithmeticCoding
{
    public class Analysis
    {
        public Analysis()
        {
            Result = new Tuple<string, double>[6];
        }

        private Tuple<string, double>[] Result;

        public void Count(Dictionary<string, string> code, Dictionary<string, double> probCode, int alpLength,
            int smsLength)
        {
            Result[0] = new Tuple<string, double>
            (
                "AverageLength_ofCodeWord",
                CountAverageLength_ofCodeWord(code.Values.ToArray<string>(), probCode.Values.ToArray())
            );
            Result[1] = new Tuple<string, double>
            (
                "AverageLength_ofCodeWord",
                CountAverageLength_perSymbol(Result[0].Item2, smsLength)
            );
            Result[2] = new Tuple<string, double>
            (
                "CountHmax",
                CountHmax(alpLength)
            );
            Result[3] = new Tuple<string, double>
            (
                "CountH1",
                CountH1(probCode.Values.ToArray<double>())
            );
            Result[4] = new Tuple<string, double>
            (
                "CountSourceRedundancy",
                CountSourceRedundancy(probCode.Values.ToArray<double>())
            );
            Result[5] = new Tuple<string, double>
            (
                "CountCodeRedundancy",
                CountCodeRedundancy(code.Values.ToArray<string>(), probCode.Values.ToArray())
            );

        }

        private double CountAverageLength_ofCodeWord(string[] code, double[] probCode)
        {
            return code.Select((t, i) => t.Length * probCode[i]).Sum();
        }

        private double CountAverageLength_perSymbol(double AverageLength_ofCodeWord, int smsLength)
            => AverageLength_ofCodeWord / smsLength;

        private double CountHmax(int alpLength) => Math.Log(alpLength, 2);

        private double CountH1(double[] prob)
        {
            double multiple = prob.Where(i => i > 0).Aggregate((a, b) => a * b);
            if (multiple == 0)
                return 0;

            return Math.Log(Convert.ToDouble(1 / multiple), 2);
        }

        private double CountSourceRedundancy(double[] prob)
        {
            var entropy = prob.Where(a => a > 0).Sum(b => b * Math.Log(b, 2));

            return (CountHmax(prob.Length) + entropy) / CountHmax(prob.Length);
        }

        private double CountCodeRedundancy(string[] code, double[] probCode)
            => (CountHmax(code.Length) - CountAverageLength_ofCodeWord(code, probCode)) / CountHmax(code.Length);

        public Tuple<string, double>[] GetResult() => Result;
    }
}
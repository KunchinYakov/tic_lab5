﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuffmanArithmeticCoding
{
    public class Grouping
    {
        private string[] _Data;

        public string[] Result
        {
            get => _Data ?? null;
            private set { }
        }

        public void GroupBy(string[] alphabet, int groupLength)
        {
            int increaseLength = groupLength - alphabet[0].Length;
            _Data = new string[alphabet.Length];
            alphabet.CopyTo(_Data, 0);

            for (int i = 0; i < increaseLength; i++)
            {
                string[] localArr = new string[_Data.Length * alphabet.Length];
                for (int j = 0, g = 0; j < _Data.Length; j++)
                {
                    for (int k = 0; k < alphabet.Length; k++, g++)
                    {
                        localArr[g] = _Data[j] + alphabet[k];
                    }
                }

                _Data = localArr;
            }
        }
    }
}

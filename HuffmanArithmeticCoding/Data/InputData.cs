﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HuffmanArithmeticCoding
{
    public class InputData
    {
        public InputData(string pathText, string pathAlphabet)
        {
            InitializeData(pathText, pathAlphabet);
        }

        public string Text;
        public string[] Alphabet;

        private void InitializeData(string pathText, string pathAlphabet)
        {
            InitializeText(pathText);
            InitializeAlphabet(pathAlphabet);
        }

        private void InitializeText(string pathText)
        {
            using (StreamReader readText = new StreamReader(pathText, Encoding.Default))
            {
                Text = readText.ReadToEnd();
            }
        }

        private void InitializeAlphabet(string pathAlphabet)
        {
            char[] charAlph;
            using (StreamReader readText = new StreamReader(pathAlphabet, Encoding.Default))
            {
                charAlph = readText.ReadToEnd().ToCharArray();
            }
            
            Alphabet = new string[charAlph.Length];
            for (int i = 0; i < charAlph.Length; i++)
            {
                Alphabet[i] = charAlph[i].ToString();
            }
        }
    }
}

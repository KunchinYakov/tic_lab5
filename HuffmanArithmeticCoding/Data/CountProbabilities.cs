﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HuffmanArithmeticCoding
{
    public class CountProbabilities
    {
        private Dictionary<string, double> Data;
        public Dictionary<string, double> Result
        {
            get => Data ?? null;
            private set {}
        }

        public void Count(string text, string[] alphabet)
        {
            int groupSize = alphabet[0].Length;
            Data = new Dictionary<string, double>();
            for (int i = 0; i < alphabet.Length; i++)
                Data.Add(alphabet[i], 0);

            for (int i = 0; i < text.Length - (groupSize - 1); i++)
            {
                string subStr = text.Substring(i, groupSize);
                Data[subStr]++;
            }

            for (int i = 0; i < alphabet.Length; i++)
                Data[alphabet[i]] = Convert.ToDouble(Data[alphabet[i]]) / Convert.ToDouble(text.Length);
        }
    }
}

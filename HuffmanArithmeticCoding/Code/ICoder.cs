﻿using System.Collections.Generic;

namespace HuffmanArithmeticCoding
{
    public interface ICoder
    {
        Dictionary<string, string> Code(Dictionary<string, double> lettersProb);
    }
}
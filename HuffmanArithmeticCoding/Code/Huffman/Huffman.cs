﻿using System;
using System.Collections.Generic;

namespace HuffmanArithmeticCoding
{
    public class Huffman : ICoder
    {
        public Huffman(int groupLength)
        {
            GroupLength = groupLength;
        }

        public int GroupLength { get; set; }

        public Dictionary<string, string> Code(Dictionary<string, double> lettersProb)
        {
            List<Node> dataHuffmanList = CreateDataHuffmanList(lettersProb);

            Node tree = BuildTree(dataHuffmanList);

            return CreateCode(tree);
        }

        private List<Node> CreateDataHuffmanList(Dictionary<string, double> letterProb)
        {
            var data = new List<Node>();

            foreach (var i in letterProb)
                data.Add(new Node(i.Key, i.Value));

            return data;
        }

        private Node BuildTree(List<Node> dataHuffmanList)
        {
            var dataHuffman = new SortedSet<Node>(dataHuffmanList, new NodeCompare());
            while (dataHuffman.Count > 1)
            {
                Node minMinNode = dataHuffman.Min;
                dataHuffman.Remove(dataHuffman.Min);

                Node minNode = dataHuffman.Min;
                dataHuffman.Remove(dataHuffman.Min);

                Node parent = new Node(minMinNode.Data + minNode.Data,
                    minNode.p + minMinNode.p,
                    minNode,
                    minMinNode);
                dataHuffman.Add(parent);
            }

            return dataHuffman.Max;
        }

        private Dictionary<string, string> CreateCode(Node tree)
        {
            var codes = new Dictionary<string, string>();
            for (int i = 0; i < tree.Data.Length; i += GroupLength)
                codes.Add(tree.Data.Substring(i, GroupLength), "");

            var nodesStack = new Stack<Node>();
            nodesStack.Push(tree);
            tree.isUsed = true;

            while (nodesStack.Count != 0)
            {
                Node topElem = nodesStack.Peek();
                if (topElem.LeftChild != null && topElem.LeftChild.isUsed == false)
                {
                    UpdateNodesStack(nodesStack, topElem.LeftChild);
                    UpdateCodes(codes, topElem.LeftChild.Data, 1);
                    continue;
                }

                if (topElem.RightChild != null && topElem.RightChild.isUsed == false)
                {
                    UpdateNodesStack(nodesStack, topElem.RightChild);
                    UpdateCodes(codes, topElem.RightChild.Data, 0);
                    continue;
                }

                nodesStack.Pop();
            }

            return codes;
        }

        private void UpdateNodesStack(Stack<Node> nodesStack, Node child)
        {
            nodesStack.Push(child);
            child.isUsed = true;
        }

        void UpdateCodes(Dictionary<string, string> codes, string data, byte codePart)
        {
            for (int i = 0; i < data.Length; i += GroupLength)
                codes[data.Substring(i, GroupLength)] += codePart;
        }
    }
}
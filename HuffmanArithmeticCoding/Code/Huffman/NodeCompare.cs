﻿using System.Collections.Generic;

namespace HuffmanArithmeticCoding
{
    internal class NodeCompare : IComparer<Node>
    {
        public int Compare(Node node1, Node node2)
        {
            if (node1?.Data == node2?.Data)
                return 0;

            if (node1?.p > node2?.p)
                return 1;

            if (node1?.p < node2?.p)
                return -1;

            if (node1?.Data.Length > node2?.Data.Length)
                return 1;

            if (node1?.Data.Length < node2?.Data.Length)
                return -1;

            if (node1?.Data.GetHashCode() > node2?.Data.GetHashCode())
                return 1;

            return -1;
        }
    }
}
﻿namespace HuffmanArithmeticCoding
{
    internal class Node
    {
        public Node(string data, double p)
        {
            this.Data = data;
            this.p = p;
        }

        public Node(string data, double p, Node leftChild, Node rightChild)
            : this(data, p)
        {
            this.LeftChild = leftChild;
            this.RightChild = rightChild;
        }

        public string Data { get; set; }
        public double p { get; set; }
        public Node LeftChild { get; set; }
        public Node RightChild { get; set; }
        public bool isUsed { get; set; }

    }
}
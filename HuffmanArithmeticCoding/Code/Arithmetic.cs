﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HuffmanArithmeticCoding
{
    public class Arithmetic : ICoder
    {
        public Arithmetic(string text)
        {
            Text = text;
            Intervals = new Tuple<decimal, decimal>[text.Length];
        }

        public string Text { get; set; }

        private Tuple<decimal, decimal>[] Intervals; //(begin of interval, end of interval)

        /// <returns> Accurate to 20 characters</returns>
        public Dictionary<string, string> Code(Dictionary<string, double> lettersProb)
        {
            char[] text = Text.ToCharArray();

            decimal startInterval = 0;
            decimal finishInterval = 1;

            Intervalization(lettersProb, startInterval, finishInterval);

            for (int i = 0; i < lettersProb.Count; i++)
            {
                startInterval = Intervals[i].Item1;
                finishInterval = Intervals[i].Item2;
                Intervalization(lettersProb, startInterval, finishInterval);
            }

            string code = ConvertToBinary(startInterval);

            return new Dictionary<string, string>
            {
                {Text, code}
            };
        }

        private void Intervalization(Dictionary<string, double> lettersProb,
            decimal startInterval, decimal finishInterval)
        {
            decimal shift = startInterval;
            decimal diapason = finishInterval - startInterval;
            for (int i = 0; i < Intervals.Length; i++)
            {
                var finish = shift + diapason * (decimal) lettersProb[Text[i].ToString()];
                Intervals[i] = new Tuple<decimal, decimal>(shift, finish);
                shift += diapason * (decimal) lettersProb[Text[i].ToString()];
            }
        }

        private string ConvertToBinary(decimal number)
        {
            const int accurate = 20;

            string ans = Convert.ToString((int) Math.Floor(number), 2) + ".";
            number -= Math.Floor(number);

            while (number != Math.Floor(number) && ans.Length < accurate)
            {
                number *= 2;
                ans += Math.Floor(number);
                number -= Math.Floor(number);
            }

            return ans;
        }
    }
}
﻿using System;
using System.Linq;

namespace HuffmanArithmeticCoding
{
    public static class Extensions
    {
        public static string ToReverseString(this byte[] arr)
        {
            Array.Reverse(arr);
            return string.Concat<byte>(arr);
        }
    }
}
